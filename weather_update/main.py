from typer import Typer
from .api import get_city_temperature

app = Typer()

@app.command()
def temperature(city: str):
    """Get temperature from city."""
    get_city_temperature(city)


