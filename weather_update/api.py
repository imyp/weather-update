from requests import get

def get_city_woe(city_name):
    url = f"https://www.metaweather.com/api/location/search/?query={city_name}"
    cities = get(url).json()
    if len(cities) == 0:
        print(f"No city was found with the following city name: {city_name}.")
        city = {}
    elif len(cities) > 1:
        city = choose_city(cities)
    else:
        city = cities[0]
    return city.get("woeid")

def choose_city(cities):
    for i, city in enumerate(cities):
        title = city.get("title")
        print(f"{i} : {title}")
    city_index_str = input("Choose city number: ")
    if not city_index_str.isnumeric():
        print("Input must be a number.")
    else:
        city_index = int(city_index_str)
        if 0 <= city_index <= len(cities):
            return cities[city_index]
        else:
            print(f"City number must be between 0 and {len(cities) - 1} (both numbers included).")

def get_city_temperature(city_name:str):
    woeid = get_city_woe(city_name)
    if woeid:
        url = f"https://www.metaweather.com/api/location/{woeid}/"
        info = get(url).json()
        title = info.get("title")
        weather = info.get("consolidated_weather")
        if len(weather)>=1:
            temperature = weather[0].get("the_temp")
            print(f"The temperature in {title} is {temperature} degrees.")
